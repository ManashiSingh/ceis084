
#include <stdio.h>
 
int main()
{
   int p, q, *a, *b, temp;
 
   printf("Enter the value of p and q\n");
   scanf("%d%d", &p, &q);
 
   printf("Before Swapping\nx = %d\ny = %d\n", p, q);
 
   a = &p;
   b = &q;
 
   temp = *b;
   *b = *a;
   *a = temp;
 
   printf("After Swapping\nx = %d\ny = %d\n", p, q);
 
   return 0;
}